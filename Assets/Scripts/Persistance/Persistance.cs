﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Persistance : MonoBehaviour
{
    public static PersonajeAtributos LeerObjetos()
    {
        string filePath = Application.dataPath + "/personaje.json";
        string jsonString = File.ReadAllText(filePath);
        PersonajeAtributos personajeAtributos = JsonUtility.FromJson<PersonajeAtributos>(jsonString);
        return personajeAtributos;
    }

    public static GuardarTecnologias LeerTecnologias()
    {
        string filePath = Application.dataPath + "/tecnologia.json";
        string jsonString = File.ReadAllText(filePath);
        GuardarTecnologias TecnologiasAtributes = JsonUtility.FromJson<GuardarTecnologias>(jsonString);
        return TecnologiasAtributes;
    }

    public static void EscribirPersonaje(PersonajeAtributos personajeAtributos)
    {
        string filePath = Application.dataPath + "/personaje.json";
        string jsonString;
        jsonString = JsonUtility.ToJson(personajeAtributos);
        File.WriteAllText(filePath, jsonString);
    }

    public static void EscribirTecnologia(GuardarTecnologias tecnologias)
    {
        string filePath = Application.dataPath + "/tecnologia.json";
        string jsonString;
        jsonString = JsonUtility.ToJson(tecnologias);
        File.WriteAllText(filePath, jsonString);
    }
}
