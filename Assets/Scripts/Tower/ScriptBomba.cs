using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class ScriptBomba : MonoBehaviour
{
    protected float Animation;

    public GameObject origin;
    public GameObject enemy;

    public Vector3 position;

    public bool noTocaSuelo = true;

    public EnemyScript enemyScript;
    public DetectarEnemigo detectarEnemigo;

    public List<GameObject> enemys;
    private EnemyScript _enemyScript;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name == "Bomba(Clone)") {
            Destroy(gameObject, 5);
        }
        position = transform.position;
        if (gameObject.transform.parent != null)
        {
            detectarEnemigo = gameObject.transform.parent.GetChild(1).GetComponent<DetectarEnemigo>();
        }

        if (detectarEnemigo == null)
        {
            Debug.Log("Es nulo");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy == null)
        {
            enemy = detectarEnemigo.EnemiesList[0].gameObject;
        }

        if (noTocaSuelo)
        {
            Animation += Time.deltaTime;
            Animation = Animation % 2;
            transform.position = MathParabola.Parabola(position, enemy.transform.position, 5f, Animation / 2f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "map")
        {
            noTocaSuelo = false;
            AreaDamage();
            Destroy(gameObject);
        }

        if (other.tag == "enemy")
        {
            enemys.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "enemy")
        {
            enemys.Remove(other.gameObject);
        }
    }

    private void AreaDamage()
    {
        Debug.Log(enemys.Count);
        for (int i = 0; i < enemys.Count; i++)
        {
            _enemyScript = enemys[i].GetComponent<EnemyScript>();
            _enemyScript.vida--;
        }
    }
}