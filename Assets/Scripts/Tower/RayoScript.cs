﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using UnityEngine;

public class RayoScript : MonoBehaviour
{
    // Start is called before the first frame update
    public EnemyScript enemyScript;
    public DetectarEnemigo detectarEnemigo;
    private CapsuleCollider _capsuleCollider;
    public Transform target;
    public int numeroRebotes = 2;
    private int indiceLista = 0;
    GameObject chocado = null;

    void Start()
    {
        _capsuleCollider = GetComponent<CapsuleCollider>();
        Destroy(gameObject, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {

        if (gameObject.transform.parent != null)
        {
            detectarEnemigo = gameObject.transform.parent.GetChild(1).GetComponent<DetectarEnemigo>();
        }
        target = detectarEnemigo.EnemiesList[indiceLista].transform;
        Debug.Log(target.position);
        transform.position = Vector3.MoveTowards(transform.position, target.position, 10f*Time.deltaTime);
    }
    

    private void OnTriggerEnter(Collider intruso)
    {

        if (intruso.gameObject.tag == "enemy" && intruso.gameObject != chocado)
        {
            chocado = intruso.gameObject;
                enemyScript = intruso.GetComponent<EnemyScript>();
                enemyScript.damaged();
                if (enemyScript.vida <= 0f)
                {
                    detectarEnemigo.EnemiesList.Remove(intruso.gameObject);
                    StartCoroutine(destruirAlEnemigo(intruso.gameObject));

                    enemyScript = null;
                    target = null;
                }

                if (detectarEnemigo.EnemiesList.Count < indiceLista)
                {
                     indiceLista++;
                }
                indiceLista++;
                target = detectarEnemigo.EnemiesList[indiceLista].transform;
                numeroRebotes--;
                if (numeroRebotes <= 0 || target == null)
                {
                    Destroy(gameObject);
                }
            }
    }

    IEnumerator destruirAlEnemigo(GameObject enemigo)
    {
        Debug.Log("enemigo destruido?");
        Destroy(enemigo.gameObject);
        yield return new WaitForSeconds(0.1f);
    }
}