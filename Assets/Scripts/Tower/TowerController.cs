﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : MonoBehaviour
{
    private GameObject tower;
    public float distance = 20000f;
    public LayerMask layer;
    public GameObject towerPrefab;
    RaycastHit hit;
    public GameObject torreArquera;
    Vector3 position;
    public Text Text;
    public GameObject tesla;
    public GameObject torretaDefinitiva;
    public GameObject bomba;

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawLine(ray.origin, hit.point, Color.blue);
        Physics.Raycast(ray, out hit, distance, layer);

        if (tower != null)
        {
            position = hit.point;

            position = new Vector3(Mathf.Floor(position.x), position.y, Mathf.Floor(position.z));

            tower.transform.position = new Vector3(position.x, 0.5f, position.z);
        }

        if (Input.GetKeyDown(KeyCode.N) && tower != null)
        {
            Text.gameObject.SetActive(false);
            Instantiate(torretaDefinitiva, new Vector3(Mathf.Floor(position.x), -0.44f, Mathf.Floor(position.z)),
                new Quaternion(0f, 0f, 0f, 0f));
            Destroy(tower);
            tower = null;
            GameManager._instance.Chatarra -= 10;
        }
    }

    public void OnArqueraClicked()
    {
        if (GameManager._instance.Chatarra >= 10)
        {
            Text.gameObject.SetActive(true);
            if (tower == null)
            {
                tower = Instantiate(towerPrefab,
                    new Vector3(position.x, position.y, position.z), new Quaternion(0f, 0f, 0f,
                        0f));
            }

            torretaDefinitiva = torreArquera;
        }
    }

    public void OnTeslaClicked()
    {
        if (GameManager._instance.Chatarra >= 10)
        {
            Text.gameObject.SetActive(true);
            if (tower == null)
            {
                tower = Instantiate(towerPrefab,
                    new Vector3(position.x, position.y, position.z), new Quaternion(0f, 0f, 0f,
                        0f));
            }

            torretaDefinitiva = tesla;
        }
    }

    public void onBombaClicked()
    {
        if (GameManager._instance.Chatarra >= 10)
        {
            Text.gameObject.SetActive(true);
            if (tower == null)
            {
                tower = Instantiate(towerPrefab,
                    new Vector3(position.x, position.y, position.z), new Quaternion(0f, 0f, 0f,
                        0f));
            }

            torretaDefinitiva = bomba;
        }
    }
}