﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarEnemigo : MonoBehaviour
{
    public ArmaDeLaTorre parentScript;

    // Start is called before the first frame updateç
    public float Timer = 0;
    public List<GameObject> EnemiesList = new List<GameObject>();
    public float cooldown;
    public TurretsAttributes ta;
    private SphereCollider _sphereCollider;

    void Start()
    {
        parentScript = this.transform.parent.GetComponent<ArmaDeLaTorre>();
        cooldown = ta.velocidadAtaque;
        _sphereCollider = GetComponent<SphereCollider>();
        _sphereCollider.radius = ta.rango;

    }

    // Update is called once per frame
    void Update()
    {
    }


    void OnTriggerStay(Collider intruso)
    {
        if (intruso.tag == "enemy")
        {
            if (Timer <= Time.time)
            {
                parentScript.Fire();
                Timer += Time.time;
                Timer += cooldown;
            }
         
        }
    }

    private void OnTriggerEnter(Collider intruso)
    {
        if (intruso.tag == "enemy")
        {
            EnemiesList.Add(intruso.gameObject);
        }
    }

    private void OnTriggerExit(Collider intruso)
    {
        if (intruso.tag == "enemy")
        {
            EnemiesList.Remove(intruso.gameObject);
        }
    }
}