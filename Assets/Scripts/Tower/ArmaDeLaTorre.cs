﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmaDeLaTorre : MonoBehaviour
{
    public GameObject Enemy;
    public float bulletSpeed = 30f;
    public GameObject bullet;
    private Transform armaDeLaTorre;
    public DetectarEnemigo detectarEnemigo;
    private Rigidbody bulletClone;
    public float vida;
    public GameObject ArmaDeLaTorreta;
    public Slider Slider;
    public TurretsAttributes ta;


    // Start is called before the first frame update
    void Start()
    {
        vida = ta.vida;
        Enemy = GameObject.FindWithTag("enemy");
        armaDeLaTorre = gameObject.transform.GetChild(0);
        detectarEnemigo = gameObject.transform.GetChild(1).GetComponent<DetectarEnemigo>();
        Slider.maxValue = vida;
    }

    // Update is called once per frame
    void Update()
    {
        if (detectarEnemigo.EnemiesList.Count > 0 && detectarEnemigo.EnemiesList[0] != null)
        {
            armaDeLaTorre.LookAt(detectarEnemigo.EnemiesList[0].transform);
        }

        if (vida <= 0)
        {
            Destroy(gameObject);
        }

        Slider.value = vida;
    }

    public void Fire()
    {
        GameObject flecha = Instantiate(bullet, gameObject.transform);
        flecha.transform.position = ArmaDeLaTorreta.transform.position;
        detectarEnemigo.Timer = 0f;
    }
}