﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;

public class ScriptFlecha : MonoBehaviour
{
    // Start is called before the first frame update
    public EnemyScript enemyScript;
    public DetectarEnemigo detectarEnemigo;
    public SphereCollider _SphereCollider;


    void Start()
    {
        _SphereCollider = GetComponent<SphereCollider>();
        Destroy(gameObject, 1.5f);
    }



    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.parent != null)
        {
            detectarEnemigo = gameObject.transform.parent.GetChild(1).GetComponent<DetectarEnemigo>();
        }

        if (detectarEnemigo.EnemiesList.Any())
        {
            transform.position = Vector3.MoveTowards(transform.position, detectarEnemigo.EnemiesList[0].transform.position, 1f);
        }
        
    }

 


    private void OnTriggerEnter(Collider intruso)
    {
        if (intruso.gameObject.tag == "enemy")
        {
            enemyScript = intruso.GetComponent<EnemyScript>();
            enemyScript.damaged();
            Destroy(gameObject);
            if (enemyScript.vida <= 0f)
            {
                detectarEnemigo.EnemiesList.Remove(intruso.gameObject);
                StartCoroutine(destruirAlEnemigo(intruso.gameObject));

                enemyScript = null;
            }
        }
    }

    IEnumerator destruirAlEnemigo(GameObject enemigo)
    {
        Debug.Log("enemigo destruido?");
        Destroy(enemigo.gameObject);
        yield return new WaitForSeconds(0.1f);
    }
}