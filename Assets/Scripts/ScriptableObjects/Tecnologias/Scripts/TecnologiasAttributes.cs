﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurretsTecnologies
{
    hp,
    speedAttack,
    damage,
    shootRange,
    nArrows,
    rExplosion,
    nJumps,
    specialAbility
}
public enum CharacterTecnologies
{
    life,
    constructionSpeed,
    atackSpeed,
    damage,
    sAreaAtack,
    sMine,
    sRepairAll,
    sSprint
}


public enum turretTipe
{
    archer,
    bomber,
    tesla,
    character
}
[Serializable]
[CreateAssetMenu(menuName = "ScriptableObjects/Tecnology/Attributes")]
public class TecnologiasAttributes : ScriptableObject
{
    public String name;
    public Sprite icon;
    public float increase;
    public bool active;

    public TurretsTecnologies Tecnology;
    public CharacterTecnologies CTecnology;

    
    public turretTipe turretTipe;
}

