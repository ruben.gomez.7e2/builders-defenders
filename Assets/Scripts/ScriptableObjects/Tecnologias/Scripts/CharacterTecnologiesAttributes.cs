﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/Tecnology/Character Attributes")]
public class CharacterTecnologiesAttributes : TecnologiasAttributes
{
    public List<CharacterTecnologiesAttributes> parent;
    public List<CharacterTecnologiesAttributes> childsTecnologies;
}

