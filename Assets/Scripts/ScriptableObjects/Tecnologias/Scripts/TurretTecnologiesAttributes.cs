﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/Tecnology/Turret Attributes")]
[Serializable]
public class TurretTecnologiesAttributes : TecnologiasAttributes
{
    

    public List<TurretTecnologiesAttributes> parent;
    public List<TurretTecnologiesAttributes> childsTecnologies;
}
