﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Characters/MainCharacterScriptable")]
public class MainCharacterScriptable : CharactersAttributes
{

    /**
     * atributos hijo(principal)
     */
    

    public int _nivel;
    public float velocidadConstruccion;
    public int experienciaTotal;
    public List<Tecnologia> listaTecnologias;
    public int RondaRecord;


    
    public void updateExperiencia(int exp)
    {
        experienciaTotal += exp;
    }


}

public class Tecnologia
{
    public String _nombreTecnologia;
    public bool _activa;
}

