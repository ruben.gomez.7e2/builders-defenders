﻿
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Characters/Attributes")]
public class CharactersAttributes : ScriptableObject
{
    /**
     * atributos padre(personajes)
     */
    public float vida;
    public float velocidadMovimiento;
    public float velocidadAtaque;
    public float daño;
    
    public int tipo;
    
}
