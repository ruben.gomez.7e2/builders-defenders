﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TurretsAttributes : ScriptableObject
{
    /**
     * atributos padre(personajes)
     */
    public float vida;
    public float rango;
    public float velocidadAtaque;
    public float daño;
    public int precio;
}
