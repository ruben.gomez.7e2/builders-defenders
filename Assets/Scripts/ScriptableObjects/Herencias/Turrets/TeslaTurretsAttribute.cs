﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ScriptableObjects/Tecnology/TeslaTurretAttributes")]

public class TeslaTurretsAttribute : TurretsAttributes
{
   public int _nivel;
   public int _rangoMinimo;
   public int _saltosEnemigos;
}
