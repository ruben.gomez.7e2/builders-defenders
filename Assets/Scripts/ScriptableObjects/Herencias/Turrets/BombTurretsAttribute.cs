﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ScriptableObjects/Tecnology/BombTurretAttributes")]
public class BombTurretsAttribute : TurretsAttributes
{
    public int _nivel;
    public int _rangoMinimo;
    public int _radioExplosionet;
}
