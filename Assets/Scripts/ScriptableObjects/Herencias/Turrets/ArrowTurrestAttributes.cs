﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ScriptableObjects/Tecnology/ArcherTurretAttributes")]
public class ArrowTurrestAttributes : TurretsAttributes
{
    public int _nivel;
    public int _rangoMinimo ;
    public int _cantidadFlechas;
    
}