﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class TecnologiesManager : MonoBehaviour
{
    public List<TecnologiasAttributes> lista;

    public ArrowTurrestAttributes ata;
    public BombTurretsAttribute bta;
    public TeslaTurretsAttribute tta;
    public MainCharacterScriptable ct;
    public static TecnologiesManager _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            GuardarTecnologias();
        }
    }

    public void setearMejora(TecnologiasAttributes ta){
        Debug.Log("LISTAA -- "+lista.Count);
        for(int i = 0; i<=lista.Count;i++){
            if(lista[i].name == ta.name){
                lista[i].active = true;
                switch(ta.turretTipe){
                    case turretTipe.archer:
                        mejoraArcher(lista[i]);
                        break;
                    case turretTipe.bomber:
                        mejoraBomber(lista[i]);
                        break;
                    case turretTipe.tesla:
                        mejoraTesla(lista[i]);
                        break;
                    case turretTipe.character:
                        mejoraCharacter(lista[i]);
                        break;
                }
            }
        }
    }

    

    public void mejoraArcher(TecnologiasAttributes ta){
        switch(ta.Tecnology){
            case TurretsTecnologies.nArrows:
                        ata._cantidadFlechas++;
                    break;
                case TurretsTecnologies.damage:
                        ata.daño += ta.increase;
                    break ;
                case TurretsTecnologies.hp:
                        ata.vida += ta.increase;
                    break;
                case TurretsTecnologies.shootRange:
                        ata.rango += ta.increase;
                    break;
                case TurretsTecnologies.speedAttack:
                        ata.velocidadAtaque += ta.increase;
                    break;
        }
    }


    public void mejoraBomber(TecnologiasAttributes ta){
        switch(ta.Tecnology){
            case TurretsTecnologies.rExplosion:
                        bta._radioExplosionet++;
                    break;
                case TurretsTecnologies.damage:
                        bta.daño += ta.increase;
                    break ;
                case TurretsTecnologies.hp:
                        bta.vida += ta.increase;
                    break;
                case TurretsTecnologies.shootRange:
                        bta.rango += ta.increase;
                    break;
                case TurretsTecnologies.speedAttack:
                        bta.velocidadAtaque += ta.increase;
                    break;
        }
    }


    public void mejoraTesla(TecnologiasAttributes ta){
        switch(ta.Tecnology){
            case TurretsTecnologies.nJumps:
                        tta._saltosEnemigos++;
                    break;
                case TurretsTecnologies.damage:
                        tta.daño += ta.increase;
                    break ;
                case TurretsTecnologies.hp:
                        tta.vida += ta.increase;
                    break;
                case TurretsTecnologies.shootRange:
                        tta.rango += ta.increase;
                    break;
                case TurretsTecnologies.speedAttack:
                        tta.velocidadAtaque += ta.increase;
                    break;
        }
    }



    public void mejoraCharacter(TecnologiasAttributes ta){
        switch(ta.CTecnology){
           /*case CharacterTecnologies.sAreaAtack:
                       
                    break;
                    case CharacterTecnologies.sMine:
                       
                    break;
                    case CharacterTecnologies.sSprint:
                       
                    break;
                    case CharacterTecnologies.sRepairAll:
                       
                    break;*/
                    
                case CharacterTecnologies.damage:
                        ct.daño += ta.increase;
                    break ;
                case CharacterTecnologies.life:
                        ct.vida += ta.increase;
                    break;
                case CharacterTecnologies.atackSpeed:
                        ct.velocidadAtaque += ta.increase;
                    break;
                case CharacterTecnologies.constructionSpeed:
                        ct.velocidadConstruccion += ta.increase;
                    break;
        }
    }

    public void GuardarTecnologias()
    {
        GuardarTecnologias guardarTecnologias = new GuardarTecnologias(lista);
        Persistance.EscribirTecnologia(guardarTecnologias);
    }
}
[Serializable]
public class GuardarTecnologias{
    public List<TecnologiasAttributes> lista;
    public GuardarTecnologias(List<TecnologiasAttributes> lista){
        this.lista = lista;
    }
}







