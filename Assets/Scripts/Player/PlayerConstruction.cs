﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConstruction : MonoBehaviour
{
    private bool constructionMode;
    public GameObject tower;

    private BoxCollider _boxCollider;

    private bool canDestroy = false;

    private Collider otherCollider;

    // Start is called before the first frame update
    void Start()
    {
        _boxCollider = transform.GetComponentInChildren<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            Destroy(otherCollider.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("tower"))
        {
            canDestroy = true;
            otherCollider = other;
        }
        else canDestroy = false;

        otherCollider = null;
    }
}