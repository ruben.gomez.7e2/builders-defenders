﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PlayerMovement : MonoBehaviour
{
    public MainCharacterScriptable mcs;


    public float distance = 2000f;
    private bool mouseRight;
    public LayerMask layer;
    public SphereCollider sphereBodyCollider;
    Vector3 position;
    public SphereCollider sphereAttackCollider;
    public bool attacking = true;
    bool enemyMarked = false;
    Rigidbody rb;
    public float vida;
    public float velocidadMovimiento;
    public float velocidadAtaque;
    public float daño;
    private string chatarraTag = "chatarra";
    private string vidaTag = "vida";
    public Slider healthSlider;
    private BoxCollider spawnerTower;
    public Text vidaText;
    public TurretTecnologiesAttributes tta;

    // Start is called before the first frame update
    void Start()
    {
        vida = mcs.vida;
        /*velocidadAtaque = mcs.velocidadAtaque;
        daño = mcs.daño;*/
        position = transform.position;
        rb = gameObject.GetComponent<Rigidbody>();
        spawnerTower = GetComponentInChildren<BoxCollider>();
        healthSlider.maxValue = mcs.vida;
    }

    // Update is called once per frame
    void Update()
    {
        
        inputs();
        if (!attacking)
        {
            move();
            canDestroy();
        }

        updatehealthSlider();

        isDestroyed();
    }
    private void updatehealthSlider()
    {
        healthSlider.value = vida;
        vidaText.text = vida.ToString();
    }

    private void isDestroyed()
    {
        if (vida <= 0)
        {
            GameManager._instance.setGameStates(GameManager.GameStates.stateOver);
        }
    }

    private void canDestroy()
    {
        if(vida <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void move()
    {
        // rb.transform.position = Vector3.MoveTowards(transform.position, new Vector3(position.x, 0.40f, position.z), 10f * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(position.x, 0.40f, position.z), 10f * Time.deltaTime*mcs.velocidadMovimiento); 

    }

    private void FixedUpdate()
    {
        if (mouseRight)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, distance, layer))
            {
                position = hit.point;
                transform.LookAt(new Vector3(position.x, 0.4f, position.z));
                
                //draw invisible ray cast/vector
                Debug.DrawLine(ray.origin, hit.point, Color.red);
                //log hit area to the console
                //Debug.Log(hit.point);
                if (hit.transform.tag == "enemy")
                {
                    Debug.Log("Enemy spoted");
                    enemyMarked = true;
                }
                else
                {
                    enemyMarked = false;
                    attacking = false;
                }
            

            }
        }
        
    }

    public void inputs()
    {
        mouseRight = Input.GetKeyDown(KeyCode.Mouse1);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (enemyMarked && other.CompareTag("enemy"))
        {
            attack();
        }

        if (other.CompareTag(chatarraTag))
        {
            Destroy(other.gameObject);
            GameManager._instance.Chatarra += 10;
        }

        if (other.CompareTag(vidaTag))
        {
            vida += 10;
            if (vida > 100)
            {
                vida = 100;
            }
            Destroy(other.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision){
        if(collision.gameObject.tag== "enemy"){
            vida -= 1;
        }
    }
    

    private void attack()
    {
        attacking = true;
    }

    
}
