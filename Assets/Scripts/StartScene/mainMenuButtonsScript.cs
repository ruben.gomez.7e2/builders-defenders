﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class mainMenuButtonsScript : MonoBehaviour
{
    public GameObject panelToShow;
    // Start is called before the first frame update
    void Start()
    {
        if (panelToShow != null) {
            panelToShow.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playBtn() {
        SceneManager.LoadScene("MasterScene", LoadSceneMode.Single);
    }

    public void displayPanel()
    {
        panelToShow.SetActive(true);
    }

    public void hidePanel()
    {
        panelToShow.SetActive(false);
    }

}
