﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class SkillsPopup : MonoBehaviour
{
    public GameObject logoText;

    // Start is called before the first frame update
    private void Start()
    {
        logoText.SetActive(false);
    }

    private void OnMouseOver()
    {
        logoText.SetActive(true);
    }

    private void OnMouseExit()
    {
        logoText.SetActive(false);
    }
}
