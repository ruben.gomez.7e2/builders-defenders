﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Enemies/Attributes")]
public class Attributes : ScriptableObject
{
    public int EnemyLive { get; private set; }
    public float speed;
    
}
