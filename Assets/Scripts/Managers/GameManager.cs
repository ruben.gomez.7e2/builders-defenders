﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public enum GameStates
    {
        stateStart,
        statePlaying,
        statePause,

        stateOver
        //gameWin
    }

    public static GameManager _instance;
    public GameStates gameState;
    public GameObject nexo;
    public int timer;
    private bool inCountdown = false;
    public Text _text;
    public int currCountdownValue;
    public Text text;
    public int Chatarra = 20;


    public void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
      
        }
        else
        {
            Destroy(this);
        }

        //1er sprint
        gameState = GameStates.statePlaying;
        //other sprints
        //gameState = GameStates.stateStart;
    }

    private void Start()
    {
        timer = 20;
        StartCoroutine(Timer());
    }
    void Update()
    {
        gameStateUpdate();
        estanEnemigosMuertos();
        updateChatarra();
    
    }



    private void updateChatarra()
    {
        text.text = Chatarra.ToString();
    }

    private void estanEnemigosMuertos()
    {
        if (Spawner._instance.enemigosSpawneados)
        {
            if (!Spawner._instance.EnemyList.Any())
            {
                Spawner._instance.enemigosSpawneados = false;
                timer += 1;
                Spawner._instance.round++;
                StartCoroutine(Timer());
            }
        }
    }
    private IEnumerator Timer()
    {
        inCountdown = true;
        
        currCountdownValue = timer;
        _text.gameObject.SetActive(true);
    
        while (currCountdownValue > 0)
        {
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
            _text.text = currCountdownValue.ToString();
        }

        inCountdown = false;
        _text.gameObject.SetActive(false);
        Spawner._instance.RoundSpawner();
    }

    public GameStates getGameStates()
    {
        return this.gameState;
    }

    public void setGameStates(GameStates gameStates)
    {
        this.gameState = gameStates;
        gameStateUpdate();
    }

    // Update is called once per frame
 

    public void gameStateUpdate()
    {
        switch (gameState)
        {
            case GameStates.statePlaying:
                setInStatePlaying();
                break;

            case GameStates.statePause:
                setInStatePause();
                break;

            case GameStates.stateOver:
                setInStateOver();
                SceneManager.LoadScene("KleytonStartScene", LoadSceneMode.Single);
                break;

            // case GameStates.gameWin:
            //     setInGameWin();
            //     break;

            default:
                setInStateStart();
                break;
        }
    }

    private void setInStatePlaying()
    {
    }

    private void setInStatePause()
    {
    }

    private void setInStateOver()
    {
    }

    // private void setInGameWin()
    // {
    //
    // }

    private void setInStateStart()
    {
    }
}