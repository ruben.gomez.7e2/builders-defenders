﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTowerInfoPanel : MonoBehaviour
{
    public Text towerInfoText;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void displayArcherTower()
    {
        towerInfoText.text = "Torre Arquera\n Dispara flechas";
    }

    public void displayTeslaTower()
    {
        towerInfoText.text = "Torre Tesla\n Dispara rayos";
    }

    public void displayBombTower()
    {
        towerInfoText.text = "Torre Bomba\n Dispara bombas";
    }
}
