﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private bool isPaused;
    public string currentSceneName;

    private void Start()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }

        if (isPaused)
        {
            activateMenu();
        }
        else
        {
            desactivateMenu();
        }
    }

    public void setPause(){
      isPaused = !isPaused;
    }

    //Control del canvas de pause, parada del juego y reactivacion
    void activateMenu()
    {
        this.pauseMenu.SetActive(true);
        Time.timeScale=0;
    }

    public void desactivateMenu()
    {
        this.pauseMenu.SetActive(false);
        Time.timeScale=1;
        isPaused = false;
    }

    public void reloadScene() {
        SceneManager.LoadScene(currentSceneName, LoadSceneMode.Single);
        setPause();
    }

    public void exitGame()
    {
        SceneManager.LoadScene("KleytonStartScene", LoadSceneMode.Single);
        setPause();
    }


}
