﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class btnConstruirHUD : MonoBehaviour
{
  private Animator animator;
  public GameObject towerCanvas;
  public Text towerInfoText;

    // Start is called before the first frame update
    void Start()
    {
      animator = towerCanvas.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void displayTowerInfo(){
      if(animator.GetBool("construir")==false){
        animator.SetBool("construir",true);
      }else{
        animator.SetBool("construir",false);
      }
    }

    
 }
