﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaGameStates : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Q))
        {
            GameManager._instance.setGameStates(GameManager.GameStates.statePlaying);
        }
        if(Input.GetKey(KeyCode.W))
        {
            GameManager._instance.setGameStates(GameManager.GameStates.statePause);
        }
        if(Input.GetKey(KeyCode.E))
        {
            GameManager._instance.setGameStates(GameManager.GameStates.stateOver);
        }
        if(Input.GetKey(KeyCode.R))
        {
            GameManager._instance.setGameStates(GameManager.GameStates.stateStart);
        }
    }
}
