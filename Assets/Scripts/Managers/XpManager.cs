﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XpManager : MonoBehaviour
{
    public static XpManager Instance;
    public int xpAcumulada;
    public int xPSiguienteNivel;
    public int nivel;
    public int RondaRecord;
    public int PuntosDeXp;

    private void Awake()
    {
         if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
        
    }

    private void Start()
    {
        xpAcumulada = 0;
        xPSiguienteNivel = 10;
        nivel = 1;
        RondaRecord = 0;
        PuntosDeXp = 0;
    }

    public
        void Update()
    {
        if (xpAcumulada >= xPSiguienteNivel)
        {
            nivel++;
            xpAcumulada -= xPSiguienteNivel;
            xPSiguienteNivel += 5;
            PuntosDeXp++;
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            SetPersistance();
        }
    }

    public void SetPersistance()
    {
        PersonajeAtributos personajeAtributos = new PersonajeAtributos();
        personajeAtributos.nivel = nivel;
        personajeAtributos.xpActual = xpAcumulada;
        personajeAtributos.rondaRecord = RondaRecord;
        personajeAtributos.xpSiguienteNivel = xPSiguienteNivel;
        personajeAtributos.PuntosDeXp = PuntosDeXp;
        Persistance.EscribirPersonaje(personajeAtributos);
    }
}

[System.Serializable]
public class PersonajeAtributos
{
    public int nivel;
    public int xpActual;
    public int rondaRecord;
    public int xpSiguienteNivel;
    public int PuntosDeXp;
}