﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public enum TowerStates
    {
        building,
        built,
        damaged,
        death
    }
    public TowerStates towerState { get; private set; }
    public int health { get; private set; }
    public int range { get; private set; }
    public int atkSpeed { get; private set; }
    public int damage { get; private set; }
    public int minRange { get; private set; }
    

}
