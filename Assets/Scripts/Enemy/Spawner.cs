﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Xml;
using Enemy;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public List<RoundEnemySpawner> List = new List<RoundEnemySpawner>();
    public GameObject alpha, beta, gamma;
    public List<GameObject> EnemyList = new List<GameObject>();
    public static Spawner _instance;
    public bool enemigosSpawneados = false;
   
    public int round = 0;
    private Random rand;

    private int rondaAuxiliar = 0;

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
        } else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        List.Add(new RoundEnemySpawner(4, 0, 0));
        List.Add(new RoundEnemySpawner(6, 0, 0));
        List.Add(new RoundEnemySpawner(4, 1, 0));
        List.Add(new RoundEnemySpawner(6, 2, 0));
        List.Add(new RoundEnemySpawner(4, 1, 1));
        List.Add(new RoundEnemySpawner(6, 4, 0));
        List.Add(new RoundEnemySpawner(6, 4, 1));
        List.Add(new RoundEnemySpawner(10, 4, 1));
        List.Add(new RoundEnemySpawner(10, 4, 4));
        List.Add(new RoundEnemySpawner(15, 6, 5));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            RoundSpawner();
        }
    }


    public void RoundSpawner()
    {
        System.Random r = new System.Random();
        for (int i = 0; i < List[round].alpha; i++)
        {
            EnemyList.Add(Instantiate(alpha, new Vector3(29, 0.22f, r.Next(-30, 30)),
                Quaternion.Euler(new Vector3(0, 0, 0))));
        }

        for (int i = 0; i < List[round].beta; i++)
        {
            EnemyList.Add(Instantiate(beta, new Vector3(29, 0.22f, r.Next(-30, 30)),
                Quaternion.Euler(new Vector3(0, 0, 0))));
        }

        for (int i = 0; i < List[round].gamma; i++)
        {
            EnemyList.Add(Instantiate(gamma, new Vector3(29, 0.22f, r.Next(-30, 30)),
                Quaternion.Euler(new Vector3(0, 0, 0))));
        }

        enemigosSpawneados = true;
    }

    public void enemyKilled(GameObject gameObject)
    {
        if (EnemyList.Contains(gameObject))
        {
            EnemyList.Remove(gameObject);
        }
    }
}