﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttacker : MonoBehaviour
{
    public EnemyScript enemyScript;
    private bool cooldown = false;

    // Start is called before the first frame update
    void Start()
    {
        if (enemyScript == null) {
            enemyScript = gameObject.GetComponentInParent<EnemyScript>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {

        switch (other.tag)
        {
            case "tower":
                enemyScript.chasingAlly = true;
                break;
            case "player":
                enemyScript.chasingAlly = true;
                break;
            case "nexus":
                enemyScript.chasingAlly = true;
                break;
                
        }
    }

    private void OnTriggerStay(Collider other)
    {
        switch (other.tag)
        {
            case "tower":
                if (!cooldown)
                {
                    other.GetComponent<ArmaDeLaTorre>().vida--;
                    StartCoroutine(AttackCooldown());
                    Debug.Log("Torreta dañada!");
                }
                break;
            case "player":
                if (!cooldown)
                {
                    other.GetComponent<PlayerMovement>().vida--;
                    StartCoroutine(AttackCooldown());
                    Debug.Log("Pj dañado!");
                }
                break;
            case "nexus":
                if (!cooldown)
                {
                    other.GetComponent<NexusController>().Vida--;
                    StartCoroutine(AttackCooldown());
                    Debug.Log("Pj dañado!");

                }
                break;
        }
    }

    private IEnumerator AttackCooldown()
    {
        cooldown = true;
        yield return new WaitForSeconds(0.75f);
        cooldown = false;
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "tower" || other.tag == "player" || other.tag == "nexus")
        {
            enemyScript.chasingAlly = false;
        }
    }
}

