﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyDetector : MonoBehaviour
{
    public SphereCollider enemyDetector;
    public EnemyScript enemyScript;
    
    // Start is called before the first frame update
    void Start()
    {
        enemyScript = gameObject.GetComponentInParent<EnemyScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "tower":
                Debug.Log("tower detected");
                enemyScript.chasingAlly = true;
                enemyScript.allyDetected = other.gameObject;
                break;
            case "Player":
                enemyScript.chasingAlly = true;
                enemyScript.allyDetected = other.gameObject;
                break;
            case "nexus":
                enemyScript.chasingAlly = true;
                enemyScript.allyDetected = other.gameObject;
                break;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        switch (other.tag)
        {
            case "tower":
                enemyScript.chasingAlly = false;
                enemyScript.allyDetected = null;
                break;
            case "player":
                enemyScript.chasingAlly = false;
                enemyScript.allyDetected = null;
                break;
            case "nexus":
                enemyScript.chasingAlly = false;
                enemyScript.allyDetected = null;
                break;
        }
    }
}
