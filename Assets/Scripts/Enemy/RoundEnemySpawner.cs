﻿using System;

namespace Enemy
{
    [Serializable]
    public class RoundEnemySpawner
    {
        public int alpha, beta, gamma;

        public RoundEnemySpawner(int alpha, int beta, int gamma)
        {
            this.alpha = alpha;
            this.beta = beta;
            this.gamma = gamma;
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
