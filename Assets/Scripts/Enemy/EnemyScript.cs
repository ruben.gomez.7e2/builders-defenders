﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;


public class EnemyScript : MonoBehaviour
{
    public float speed;
    public float vida;
    public bool chasingAlly = false;
    public GameObject allyDetected;
    public GameObject cristalDeVida;
    public GameObject cristalDeChatarra;
    public GameObject gammaEnemy;

    

    public CharactersAttributes ca;

    // Start is called before the first frame update
    void Start()
    {
        vida = ca.vida;
    }

    // Update is called once per frame
    void Update()
    {
        if (vida <= 0)
        {
            Destroy(gameObject);
        }

        //Esto hace que el personaje no camine de espaldas
        if (gammaEnemy != null && (gammaEnemy.transform.localScale.x>0 && gammaEnemy.transform.localScale.z>0)) {
            gammaEnemy.transform.localScale = new Vector3(-gammaEnemy.transform.localScale.x, gammaEnemy.transform.localScale.y, -gammaEnemy.transform.localScale.z);
        }
    }

    public void damaged()
    {
        vida--;
    }

    private void FixedUpdate()
    {
        moveToCenter();
    }

    private void moveToCenter()
    {
        
        if (!chasingAlly)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, transform.position.y, 0),
                speed * Time.deltaTime);
            transform.LookAt(new Vector3(0, transform.position.y, 0));
        }
        else if (allyDetected != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, allyDetected.transform.position,
                speed * Time.deltaTime);
            transform.LookAt(allyDetected.transform);

        }

        //else chasingAlly = false;
    }

    private void OnDestroy()
    {
        if(ca.tipo== 1){
            XpManager.Instance.xpAcumulada += 1;
        } else if (ca.tipo == 2){
            XpManager.Instance.xpAcumulada += 2;
        } else if(ca.tipo == 3){
            XpManager.Instance.xpAcumulada += 1;     
        } 
        Spawner._instance.enemyKilled(this.gameObject);
        Random rnd = new Random();
        int rd = rnd.Next(2);
        if (rd == 0)
        {
            Instantiate(cristalDeVida, transform.position, transform.rotation);
        }
        else
        {
            Instantiate(cristalDeChatarra, transform.position, transform.rotation);
        }
    }
}