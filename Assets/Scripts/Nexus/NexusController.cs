﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class NexusController : MonoBehaviour
{
    public int Vida;

    public Slider Slider;
    // Start is called before the first frame update
    void Start()
    {
        Slider.maxValue = Vida;
    }

    // Update is called once per frame
    void Update()
    {
        comprovarVida();
        Slider.value = Vida;
    }

    private void comprovarVida()
    {
        if (Vida <= 0)
        {
            GameManager._instance.setGameStates(GameManager.GameStates.stateOver);
        }
    }
    
}
