﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class CameraMovement : MonoBehaviour
{
    public GameObject targetObject;
    private float targetAngle = 0;
    const float rotationAmount = 7.5f;
    public float rDistance = 1.0f;
    public float rSpeed = 1.0f;


    private void Start()
    {
        targetObject = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        drag();
        // Trigger functions if Rotate is requested
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            targetAngle -= 90.0f;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            targetAngle += 90.0f;
        }

        if (targetAngle != 0)
        {
            Rotate();
        }
    }
    public float dragSpeed = 1;
    private Vector3 dragOrigin;
 
 
    void drag()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }
 
        if (!Input.GetMouseButton(0)) return;
 
        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);
 
        transform.Translate(move);  
    }



    protected void Rotate()
        {
            float step = rSpeed * Time.deltaTime;
            float orbitCircumfrance = 2F * rDistance * Mathf.PI;
            float distanceDegrees = (rSpeed / orbitCircumfrance) * 360;
            float distanceRadians = (rSpeed / orbitCircumfrance) * 2 * Mathf.PI;

            if (targetAngle > 0)
            {
                transform.RotateAround(targetObject.transform.position, Vector3.up, -rotationAmount);
                targetAngle -= rotationAmount;
            }
            else if (targetAngle < 0)
            {
                transform.RotateAround(targetObject.transform.position, Vector3.up, rotationAmount);
                targetAngle += rotationAmount;
            }
        }
    }