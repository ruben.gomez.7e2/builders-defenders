﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gammaEnemyScript : MonoBehaviour
{
    public Animator animator;
    public GameObject gammaEnemy;
    private EnemyScript enemyScript;

    void Start()
    {
        enemyScript = gammaEnemy.GetComponent<EnemyScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyScript.vida <= 0 && enemyScript!=null) {
            animator.SetBool("morir", true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        animator.SetBool("atacar", true);
    }

    private void OnTriggerExit(Collider other)
    {
        animator.SetBool("atacar", false);
    }
}
