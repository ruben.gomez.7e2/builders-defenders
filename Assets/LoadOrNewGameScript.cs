﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadOrNewGameScript : MonoBehaviour
{

    public GameObject startScreen;
    private PersonajeAtributos pa;
    private GuardarTecnologias ta;

    void Start()
    {
        startScreen.SetActive(false);
    }

    public void continuarPartida()
    {
        pa = Persistance.LeerObjetos();
        ta = Persistance.LeerTecnologias();
        SetPersonajeAtributes();
        SetTecnologiasAtributes();
        this.gameObject.SetActive(false);
        startScreen.SetActive(true);
    }

    private void SetTecnologiasAtributes()
    {
        for (int i = 0; i <= ta.lista.Count; i++)
        {
            if (ta.lista[i].active)
            {
                TecnologiesManager._instance.setearMejora(ta.lista[i]);
            }
        }
    }

    private void SetPersonajeAtributes()
    {
        XpManager.Instance.nivel = pa.nivel;
        XpManager.Instance.xpAcumulada = pa.xpActual;
        XpManager.Instance.RondaRecord = pa.rondaRecord;
        XpManager.Instance.PuntosDeXp = pa.PuntosDeXp;
        XpManager.Instance.xPSiguienteNivel = pa.xpSiguienteNivel;
    }

    public void cargarPartida()
    {
        this.gameObject.SetActive(false);
        startScreen.SetActive(true);
    }
}
